
public interface SomeInterface {
	int i = 7;
	
	void intr1();
	int intr2();
	int intr3(int i);
	
	default void intr4() {
		
	}
}
