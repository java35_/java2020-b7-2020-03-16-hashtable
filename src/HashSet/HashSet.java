package HashSet;
public class HashSet {
	Node[] cars = new Node[6];
	
	
	
	public void addCar(Car car) {
		int index = Math.abs(car.hashCode()) % 6;
		if (cars[index] != null) {
			Node node = cars[index];
			while (node.next != null)
				node = node.next;
			node.next = new Node(null, car);
		} else
			cars[index] = new Node(null, car);
	}
	
	private class Node {
		Node next;
		Car value;
		
		public Node(Node next, Car value) {
			this.next = next;
			this.value = value;
		}
	}
}
