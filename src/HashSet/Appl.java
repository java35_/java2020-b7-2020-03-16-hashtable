package HashSet;

public class Appl {
	public static void main(String[] args) {
		HashSet hashSet = new HashSet();
		
		Car car = new Car("a111aa", "Model1", "Black");
		Car car1 = new Car("b222bb", "Model2");
		Car car2 = new Car("d222dd", "Model2");
		Car car3 = new Car("d232cc", "Model2");
		Car car4 = new Car("e212ee", "Model2");
		
		hashSet.addCar(car);
		hashSet.addCar(car1);
		hashSet.addCar(car2);
		hashSet.addCar(car3);
		hashSet.addCar(car4);
	}
}
